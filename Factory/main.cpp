﻿using namespace std;
#include <string>
#include <iostream>
#include <vector>
#include <queue>

enum JobSpecifications { WORKMAN, MANAGER, FINANCIER, COURIER, PROGRAMMER};
enum Languages { ENGLISH, GERMAN, FRENCH, ITALIAN, RUSSIAN, SPANISH };
enum DriverCat { A1, A2, A, B, B1, C, C1,  D };


class HardSkills {
private:
	vector<Languages> lan;
	vector<DriverCat> dCat;
public:
	void setLan(vector<Languages> lan) {
		this->lan = lan;
	}
	void setdCat(vector<DriverCat>) {
		this->dCat = dCat;
	}
	vector<Languages> getLan() {
		return lan;
	}
	vector<DriverCat> getdCat() {
		return dCat;
	}
};



class Softskills :public HardSkills {
private:
	vector<string> softSkills;
public:
	void setsoftSkills(vector <string> ss) {
		softSkills = ss;
	}
	vector<string> getss() {
		return softSkills;
	}
};


class Jobs {
private:
	int jobid;
	JobSpecifications speciality;
public:
	Jobs(int jobid, JobSpecifications s) {
		this->jobid = jobid;
		this->speciality = s;
	}
	Jobs() {
		jobid = 0;
		speciality = WORKMAN;
	}
	int getJobid() {
		return jobid;
	}
	JobSpecifications getSpec() {
		return speciality;
	}
};



class Worker {
private:
	HardSkills hSkills;
	Softskills sSkills;
	string firstName;
	string lastName;
	string hiredate;
	string birthDate;
	bool gender;
	int workExperienceInYears;
	Jobs job;

public:
	Worker(int workExperienceInYears, string firstName, string lastName, string hiredate, Jobs job, HardSkills hSkills, Softskills sSkills, string birthDate, bool gender) {
		this->firstName = firstName;
		this->lastName = lastName;
		this->hiredate = hiredate;

		this->hSkills = hSkills;
		this->sSkills = sSkills;
		this->gender = gender;
		this->birthDate = birthDate;
		this->workExperienceInYears = workExperienceInYears;
	}
	string getlastName() {
		return lastName;
	}
	string getfirstName() {
		return firstName;
	}
	string gethiredate() {
		return hiredate;
	}

	Jobs getJob() {
		return job;
	}
	HardSkills getHS() {
		return hSkills;
	}
	Softskills getSS() {
		return sSkills;
	}
	int getExperience() {
		return workExperienceInYears;
	}
};








class Good{
private:
	int id;
	string descrip;
	float stdPrice;
	float actualPrice;
	int QTY;
public:
	Good(int id, string descrip, float stdPrice,  float actualprice, int QTY){
		this->descrip = descrip;
		this->stdPrice = stdPrice;
		this->actualPrice = actualprice;
		this->QTY = QTY;
		this->id = id;
	}
	Good() {
		descrip = "";
		stdPrice = 0.0;
		actualPrice = 0.0;
		QTY = 0;
	}
	string getDescrip(){
		return descrip;
	}
	float getstdPrice(){
		return stdPrice;
	}
	float getactualPrice(){
		return actualPrice;
	}
	float getQTY() {
		return QTY;
	}
};


class Positions
{
private:
	Jobs job;
	int generalNumberOfPlaces;
	int occupied;
public:
	Positions(Jobs job, int generalNumberOfPlaces, int occupied){
		this->job = job;
		this->generalNumberOfPlaces = generalNumberOfPlaces;
		this->occupied = occupied;
	}

	Jobs getJob(){
		return job;
	}
	int getGeneralNumberOfPlaces(){
		return generalNumberOfPlaces;
	}
	int getOccuped(){
		return occupied;
	}
};

class WorkerList {
private:
	queue<Worker> list;
public:
	WorkerList(queue<Worker> list) {
		this->list = list;
	}
	queue<Worker> getWorkerList() {
		return list;
	}
};

class Factory{
protected:
	vector<Positions> p;
	WorkerList list;
	string address;
	string specialization;
	unsigned int floorSpace;
	unsigned short floorNumb;
public:


	
	virtual Good makeProduct() = 0;
	virtual void EmployWorker(Worker worker){
		
		Jobs workerJob = worker.getJob();

		for (int i=WORKMAN; i<=PROGRAMMER; ++i)
		{
			if (i==workerJob.getSpec() && p[i].getGeneralNumberOfPlaces()>p[i].getOccuped() && worker.getExperience()<1)
			{
				list.getWorkerList().push(worker);
			}
		}

	}

	void setAddress(string address){
		this->address = address;
	}
	string getAddress() {
		return address;
	}

	void setSpacialization(string specialization) {
		this->specialization = specialization;
	}
	string getSpecialization() {
		return specialization;
	}
	WorkerList getList() {
		return list;
	}

};

class WoodFactory:public Factory{
public:
	Good makeProduct()override{
		Good g();
	
		return g();
	}

	void EmployWorker(Worker worker) override{

		Jobs workerJob = worker.getJob();

		for (int i = WORKMAN; i <= PROGRAMMER; ++i)
		{
			if (i == workerJob.getSpec() && p[i].getGeneralNumberOfPlaces() > p[i].getOccuped() && worker.getExperience() < 0.5)
			{
				list.getWorkerList().push(worker);
			}
		}

	}
};

class TextilesFactory:public Factory{

public:
	Good makeProduct()override{
		Good g();

		return g();
	}

	void EmployWorker(Worker worker) override {

		Jobs workerJob = worker.getJob();

		for (int i = WORKMAN; i <= PROGRAMMER; ++i)
		{
			if (i == workerJob.getSpec() && p[i].getGeneralNumberOfPlaces() > p[i].getOccuped() && worker.getExperience() < 1.5)
			{
				list.getWorkerList().push(worker);
			}
		}

	}

};




int main(){


	return 0;
}